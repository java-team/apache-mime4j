apache-mime4j (0.8.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.8.12
  * Bump Standards-Version to 4.7.0

 -- tony mancill <tmancill@debian.org>  Sun, 29 Dec 2024 10:56:29 -0800

apache-mime4j (0.8.11-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.8.11

 -- tony mancill <tmancill@debian.org>  Thu, 07 Mar 2024 21:17:59 -0800

apache-mime4j (0.8.10-1) unstable; urgency=medium

  * Team upload
  * Update debian/watch to use GitHub API
  * New upstream version 0.8.10
    CVE-2024-21742 (Closes: #1064966)
  * Update debian/rules for different upstream changelog
  * Freshen years in debian/copyright
  * Use debhelper-compat 13
  * Set Rules-Requires-Root: no in debian/control
  * Bump Standards-Version to 4.6.2
  * Include Apache NOTICE file in binary package

 -- tony mancill <tmancill@debian.org>  Sat, 02 Mar 2024 21:55:07 -0800

apache-mime4j (0.8.2-1) unstable; urgency=medium

  * New ustream release

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 22 Jan 2019 22:49:10 +0100

apache-mime4j (0.8.1-1) unstable; urgency=medium

  * New ustream release
    - Build depend on junit4 instead of junit
    - New build dependencies on libassertj-core-java and libmockito-java
    - Ignore the new james-utils module
  * Removed the -java-doc package
  * Removed the build dependency on libmaven-install-plugin-java
  * Standards-Version updated to 4.3.0
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs
  * Track and download the new releases from GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 19 Jan 2019 12:01:25 +0100

apache-mime4j (0.7.2-4) unstable; urgency=medium

  * Build with maven-debian-helper (should improve the reproducibility)
  * Removed the dependency on maven-assembly-plugin (Closes: #804458)
  * Enabled the OSGi metadata
  * No longer build and install the benchmark module
  * Install the javadoc under the /usr/share/doc/libapache-mime4j-java/api
    directory instead of /usr/share/libapache-mime4j-java-doc
  * Register the documentation with doc-base
  * Standards-Version updated to 3.9.6 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 09 Nov 2015 10:15:28 +0100

apache-mime4j (0.7.2-3) unstable; urgency=medium

  * Team upload.
  * Maintenance transferred again to the Debian Java Team (Closes: #711840)
  * debian/control:
    - Changed the priority from extra to optional
  * Updated the watch file
  * debian/rules:
    - Preserve the apache-mime4j-project pom in the binary package
    - Added a get-orig-source target
    - Improved the clean target

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 09 Jul 2014 08:17:38 +0200

apache-mime4j (0.7.2-2) unstable; urgency=low

  * Upload to unstable
  * Fixed Vcs-* fields in debian/control
  * Updated debian/watch
  * Standards-Version bump to 3.9.5, no changes needed

 -- David Paleino <dapal@debian.org>  Sun, 03 Nov 2013 11:13:27 +0100

apache-mime4j (0.7.2-1) experimental; urgency=low

  * New upstream version.
  * Build-Depend on default-jdk (Closes: #683501).
  * Fixed debian/watch.
  * Manually build javadoc, waiting to understand how to properly
    configure maven-javadoc-plugin.
  * Standards-Version bump to 3.9.4, no changes needed.
  * Bump debhelper compatibility to 9.
  * Drop dependency on JRE.
  * Updated debian/copyright.
  * Add Class-Path to generated jars, and use javahelper to determine
    runtime dependencies.

 -- David Paleino <dapal@debian.org>  Sun, 30 Dec 2012 22:12:41 +0100

apache-mime4j (0.6.1-3) unstable; urgency=low

  * Maintenance transferred to the Debian Java Maintainers (Closes: #711840)
  * debian/control:
    - Updated Standards-Version to 3.9.4 (no changes)
    - Use canonical URLs for the Vcs-* fields
    - Removed the explicit build dependency on openjdk-6-jdk
    - Removed the dependency on the JRE for libapache-mime4j-java
    - Changed the priority from extra to optional
  * Updated the watch file
  * debian/rules: Added a get-orig-source target
  * debian/copyright: Updated the Format URI to 1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 14 Jun 2013 23:45:32 +0200

apache-mime4j (0.6.1-2) unstable; urgency=low

  * Upload to unstable

 -- David Paleino <dapal@debian.org>  Mon, 14 Feb 2011 16:10:55 +0100

apache-mime4j (0.6.1-1) experimental; urgency=low

  * New upstream version
  * debian/rules: also install the .pom file in the versioned directory
    in the Maven repository.
  * debian/copyright fixed according to current DEP-5
  * Standards-Version bumped to 3.9.1, no changes needed

 -- David Paleino <dapal@debian.org>  Sat, 15 Jan 2011 02:12:34 +0100

apache-mime4j (0.6-2) unstable; urgency=low

  * debian/rules: install the correct jarfile
  * debian/maven.rules and debian/maven.ignoreRules added: use
    mh_patchpom and mh_unpatchpoms in debian/rules, instead of patching
    the pom.xml.
  * debian/patches/00-fix_build.patch refreshed, now using mh_*patchpom*
    for most things
  * debian/docs removed -- RELEASE_NOTES.txt is installed as the
    upstream changelog

 -- David Paleino <dapal@debian.org>  Tue, 13 Jul 2010 09:28:28 +0200

apache-mime4j (0.6-1) unstable; urgency=low

  * Initial release (Closes: #585601)

 -- David Paleino <dapal@debian.org>  Thu, 08 Jul 2010 13:22:14 +0200
